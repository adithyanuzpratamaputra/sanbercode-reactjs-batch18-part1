// soal 1
console.log("-Soal Nomer 1-")
console.log()

console.log("Menghitung Luas Lingkaran")
const luaslingkaran = (p, r) => {
    let hasil = p * r * r
    return hasil
}
console.log(luaslingkaran(3.14, 4))
console.log("Menghitung Keliling Lingkaran")
const kelilinglingkaran = (p, r) => {
    let jumlah = p * r * 2
    return jumlah
}
console.log(kelilinglingkaran(3.14, 2))

console.log()

// soal 2
console.log("-Soal Nomer 2-")
console.log()

let kalimat = ""

const tambah = () => {
    const kata1 = 'Saya'
    const kata2 = 'adalah'
    const kata3 = 'seorang'
    const kata4 = 'frontend'
    const kata5 = 'developer'

    const string1 = `${kata1}`
    const string2 = `${kata2}`
    const string3 = `${kata3}`
    const string4 = `${kata4}`
    const string5 = `${kata5}`

    console.log(string1)
    console.log(string2)
    console.log(string3)
    console.log(string4)
    console.log(string5)
}

console.log(tambah())
console.log()

// soal 3
console.log("-Soal Nomer 3-")
console.log()

const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => {
            console.log(`${firstName} ${lastName}`)
            return
        }
    }
}

newFunction("William", "Imoh").fullName()
console.log()

// soal 4
console.log("-Soal Nomer 4-")
console.log()

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject

console.log(firstName, lastName, destination, occupation, spell)
console.log()

//  SOAL 5
console.log("-Soal Nomer 5-")
console.log()

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east, ]

console.log(combined)