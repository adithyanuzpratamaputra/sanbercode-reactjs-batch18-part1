// soal no.1

console.log("-Soal Nomer 1-")
console.log()

function Halo() {
    console.log("Halo Sansbers!");
}
Halo();
console.log()

// soal no.2

console.log("-Soal Nomer 2-")
console.log()

function kalikan(pertama, kedua) {
    return pertama * kedua
}

var num1 = 12
var num2 = 4

var hasilkali = kalikan(num1, num2)
console.log(hasilkali)
console.log()

// soal no.3

console.log("-Soal Nomer 3-")
console.log()

function introduce(name, age, address, hobby) {
    return "Nama Saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby
}
var name = "Adit"
var age = "20"
var address = "jln. jalan jauh"
var hobby = "jalan jalan"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)
console.log()

// soal no.4

console.log("-Soal Nomer 4-")
console.log()

var arrayDaftarPeserta = ["Adithya", "laki-laki", "jalan jalan", "2000"]
var person = {
    nama: arrayDaftarPeserta[0],
    jenis_kelamin: arrayDaftarPeserta[1],
    hobi: arrayDaftarPeserta[2],
    tahun_lahir: arrayDaftarPeserta[3]
}
console.log(person)
console.log()

// soal no.5

console.log("-Soal Nomer 5-")
console.log()

var buah = [{ namaBuah: "strawberry", warnaBuah: "merah", adaBiji: "tidak", harga: 9000 },
    { namaBuah: "jeruk", warnaBuah: "orange", adaBiji: "ada", harga: 8000 },
    { namaBuah: "semangka", warnaBuah: "hijau & merah", adaBiji: "ada", harga: 10000 },
    { namaBuah: "pisang", warnaBuah: "kuning", adaBiji: "tidak", harga: 5000 }
]
console.log(buah[0])
console.log()

// soal no.5

console.log("-Soal Nomer 6-")
console.log()

var datafilm = []

var listfilm = {
    judul: "5 cm",
    durasi: "2 jam 6 menit",
    genre: "drama",
    tahun: 2012
}

function tambahfilm(judul, durasi, genre, tahun) {
    datafilm.push(listfilm)
    return datafilm
}
tambahfilm("judul", "durasi", "genre", "tahun")
console.log(datafilm)