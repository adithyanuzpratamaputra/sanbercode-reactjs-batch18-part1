// soal 1
console.log("-Soal 1-")
console.log("Release 0")

class Animal {
    constructor(namaHewan) {
        this._name = namaHewan
        this.legs = 4
        this.goldar = false
    }
    get namaHewan() {
        return this._name
    }
    set namaHewan(a) {
        this._name = nama
    }
}

var sheep = new Animal("shaun", 4, "false")

console.log(sheep.namaHewan)
console.log(sheep.legs)
console.log(sheep.goldar)

console.log()
console.log("Release 1")

class Ape extends Animal {
    constructor(namaHewan) {
        super(namaHewan);
        this.legs = 2
        this.goldar = true
    }
    yell() {
        console.log("Auuoooo")
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell()
console.log(sungokong)

class Frog extends Animal {
    constructor(namaHewan) {
        super(namaHewan);
        this.legs = 4
        this.goldar = false
    }
    jump() {
        console.log("HopHop")
    }
}
var kodok = new Frog("buduk")
kodok.jump()
console.log(kodok)
console.log()

// soal 2
console.log("-Soal 2-")

class Clock {
    constructor({ template }) {
        this.template = template;
    }

    render() {
        var date = new Date();

        var jam = date.getHours();
        if (jam < 10) jam = '0' + jam;

        var menit = date.getMinutes();
        if (menit < 10) menit = '0' + menit;

        var detik = date.getSeconds();
        if (detik < 10) detik = '0' + detik;

        var hasil = this.template
            .replace('h', jam)
            .replace('m', menit)
            .replace('s', detik);

        console.log(hasil);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}
var clock = new Clock({ template: 'h:m:s' });
clock.start();